package Config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * The QueryConfigReader class uses the method readQuery to create object of
 * properties class,load config.properties file and return file data as stream.
 * @author Anchal.Rajput
 */

public class QueryConfigReader {
	/**
	 * The readQuery method is used to read the sql query given in config.properties
	 * file and checks if the value of sql is null or empty through is else
	 * condition.
	 * @param sql  This is they key associated with the value which we want to read.
	 * @param user This parameter is used to accept the value of sql.
	 */

	public void readQuery(String sql) {
		Properties prop = new Properties();

		try {
			prop.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
			String user = prop.getProperty(sql);
			if (user != null || !user.isEmpty()) {
				System.out.println(user);
			}

			else {
				System.out.println("user value not found");
			}
		} catch (Exception e) {
			System.out.println("file not found");
		}

	}

	
}
