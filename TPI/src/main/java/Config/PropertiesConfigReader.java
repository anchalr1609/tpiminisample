package Config;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import App.Logfile;

/**
 * The PropertiesConfigReader class retrieves the values mapped to the keys
 * defined in config.properties file.
 * 
 * @author Anchal.Rajput
 */

public class PropertiesConfigReader {
	public static String tpiUserName;
	public static String tpiPassword;
	public static String tpiPort;
	public static String tpiSampleDriver;
	public static String tpiSampleUrl;
	public static String tpifilespath;

	/**
	 * This method reads and retrieves parameters from FileMatchReader and
	 * QueryConfigReader class using there respective objects.
	 * 
	 * @param user     This is the first parameter to readParametersFromFile method
	 * @param company  This is the second parameter to readParametersFromFile method
	 * @param filename This is the third parameter to readParametersFromFile method
	 * @param sql1     This is the fourth parameter to readParametersFromFile method
	 *                 throws exception in try clause and logs exception in catch
	 *                 clause
	 * @throws SQLException
	 * @throws IOException
	 */
	public void readParametersFromFile() throws SQLException, IOException {
		Logger logger = Logger.getLogger("PropertiesConfigReader.class");

		String filename = "config.properties";
		Properties prop = new Properties();
		prop.load(getClass().getClassLoader().getResourceAsStream(filename));
		try {
			String tpiusername = prop.getProperty("TPI_TEST_USERNAME");

			if (tpiusername != null && !tpiusername.isEmpty()) {
				System.out.println(tpiusername);
			} else {

				logger.log(Level.WARNING, "Empty string!!");
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "MY log");
		}

		try {
			String tpipassword = prop.getProperty("TPI_TEST_PASSWORD");
			if (tpipassword != null && !tpipassword.isEmpty()) {
				System.out.println(tpipassword);
			} else {
				logger.log(Level.WARNING, "MY log");
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "MY log");
		}

		try {
			String tpiport = prop.getProperty("TPI_PORT");
			if (tpiport != null && !tpiport.isEmpty()) {
				System.out.println(tpiport);
			} else {
				logger.log(Level.INFO, "MY log");
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "MY log");
		}

		try {
			String tpiSampleDriver = prop.getProperty("TPI_SAMPLE_DRIVER");
			if (tpiSampleDriver != null && !tpiSampleDriver.isEmpty()) {
				System.out.println(tpiSampleDriver);
			} else {
				logger.log(Level.WARNING, "MY log");
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "MY log");
		}
		try {
			String tpiSampleUrl = prop.getProperty("TPI_SAMPLEURL");
			if (tpiSampleUrl != null && !tpiSampleUrl.isEmpty()) {
				System.out.println(tpiSampleUrl);
			} else {
				logger.log(Level.WARNING, "MY log");
			}
			String tpifilespath = prop.getProperty("TPI_FILEPATH");
			System.out.println(tpifilespath);
		} catch (Exception e) {
			logger.log(Level.WARNING, "MY log");
		}
	}
}
