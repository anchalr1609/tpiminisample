package App;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import Config.PropertiesConfigReader;

/**
 * The TPI_Class reads parameters from configuration file and prints output on
 * the console.
 * 
 * @author Anchal.Rajput
 * @version
 */

public class TPIClass {
	/**
	 * The object of PropertiesConfigReader Class calls and use the method of
	 * PopertiesConfigReader.
	 * 
	 * @exception IOException  on input error
	 * @exception SQLException on
	 */
	public static void main(String args[]) {
		
	
	File currentDir = new File("C:\\Users\\Anchal.Rajput\\eclipse-workspace\\TPI\\TPI_files"); // current directory
	start(currentDir);
	PropertiesConfigReader reader = new PropertiesConfigReader();
	
	try {
		reader.readParametersFromFile();
	} catch (SQLException e) {

	} catch (IOException ae) {

	}
	}



	public static void start(File dir) {
		
	try {
			File[] files = dir.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					System.out.println("directory:" + file.getCanonicalPath());
					start(file);
					System.out.println(file);
				}
				else
				{
					System.out.println("file:"+file.getCanonicalPath());
				}
			}
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	}
}

	
	